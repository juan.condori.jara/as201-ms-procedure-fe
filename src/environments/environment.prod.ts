export const environment = {
  production: true,
  personapi: 'http://www.deillanes.com/backend/person/person',
  studentapi: 'http://www.deillanes.com/backend/student/student',
  careerapi: 'http://www.deillanes.com/backend/career/career',
  procedureapi: 'http://www.deillanes.com/backend/procedure-consolidator/procedure'
};
